import { join, relative } from 'path';

import test from 'ava';
import fs from 'fs-extra';

import {
  afterEach,
  beforeEach,
  build,
  linkRegex,
  scriptRegex,
} from './_helpers';

test.beforeEach(beforeEach);

test.afterEach(afterEach);

test('do nothing when rollup does not write', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
  } = t.context;

  const bundle = await build({ dir });
  await bundle.generate({ format });

  t.false(await fs.pathExists(html));
});

test('write html when rollup does write', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;

  const bundle = await build({ dir });
  await bundle.write({ format, file: js });

  t.true(await fs.pathExists(html));
});

test('write html with correct relative path in script tag', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;

  const bundle = await build({ dir });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  t.regex(output, scriptRegex(scriptPath));
});

test('write html with one more script tag when `opts.moreScripts` is a string', async t => {
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreScripts = 'https://www.example.com/lib.js';

  const bundle = await build({ dir, moreScripts });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  const scripts = [moreScripts, scriptPath];
  t.plan(scripts.length);
  scripts.map(script => t.regex(output, scriptRegex(script)));
});

test('write html with more script tags when `opts.moreScripts` is an array', async t => {
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreScripts = [
    'https://www.example.com/lib-a.js',
    'https://www.example.com/lib-b.js',
    'https://www.example.com/test.js',
  ];

  const bundle = await build({ dir, moreScripts });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  const scripts = moreScripts.concat(scriptPath);
  t.plan(scripts.length);
  scripts.map(script => t.regex(output, scriptRegex(script)));
});

test('write html with correct relative path in link tag when `opts.css` is a path', async t => {
  t.plan(1);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;

  const bundle = await build({ css, dir });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  t.regex(output, linkRegex(linkPath));
});

test('write html with link tags when `opts.css` is a path and `opts.moreStyles` is a string', async t => {
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreStyles = 'https://www.example.com/framework.css';

  const bundle = await build({ css, dir, moreStyles });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const styles = [moreStyles, linkPath];
  t.plan(styles.length);
  styles.map(style => t.regex(output, linkRegex(style)));
});

test('write html with link tag when `opts.moreStyles` is a string', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreStyles = 'https://www.example.com/framework.css';

  const bundle = await build({ dir, moreStyles });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.regex(output, linkRegex(moreStyles));
});

test('write html with link tags when `opts.moreStyles` is an array', async t => {
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreStyles = [
    'https://www.example.com/reset.css',
    'https://www.example.com/framework.css',
    'https://www.example.com/grid.css',
  ];

  const bundle = await build({ dir, moreStyles });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.plan(moreStyles.length);
  moreStyles.map(style => t.regex(output, linkRegex(style)));
});

test('write html with custom filename when `opts.filename` is given', async t => {
  t.plan(4);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const filename = 'test.html';
  const actualHtml = join(dir, filename);

  const bundle = await build({ dir, filename });
  await bundle.write({ format, file: js });

  t.false(await fs.pathExists(html));
  t.true(await fs.pathExists(actualHtml));
  const output = await fs.readFile(actualHtml, 'utf8');
  t.regex(output, /^<!DOCTYPE html>/);
  t.regex(output, /<\/html>\n$/);
});

test('write html with custom title when `opts.title` is given', async t => {
  t.plan(1);
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const title = 'test';

  const bundle = await build({ dir, title });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  t.regex(output, new RegExp(`<title>${title}</title>`));
});

test('write html with correct script tag when boolean suffix is used', async t => {
  t.plan(2);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;

  const suffix = true;
  const suffixMatch = '\\?s=[0-9]{13}';
  const bundle = await build({ dir, css, suffix });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  t.regex(output, scriptRegex(scriptPath + suffixMatch));

  const linkPath = relative(dir, css);
  t.regex(output, linkRegex(linkPath + suffixMatch));
});

test('write html with correct script tag when suffix is false', async t => {
  t.plan(2);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;

  const suffix = false;
  const bundle = await build({ dir, css, suffix });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  t.regex(output, scriptRegex(scriptPath));

  const linkPath = relative(dir, css);
  t.regex(output, linkRegex(linkPath));
});

test('write html with correct script tag when string suffix is used', async t => {
  t.plan(2);
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;

  const suffix = 'v=1.2.3';
  const bundle = await build({ dir, css, suffix });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  t.regex(output, scriptRegex(`${scriptPath}\\?${suffix}`));

  const linkPath = relative(dir, css);
  t.regex(output, linkRegex(`${linkPath}\\?${suffix}`));
});

test('write html with link tags when `opts.css` is a path and `opts.moreStyles` is a string and suffix', async t => {
  const {
    css,
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreStyles = 'https://www.example.com/framework.css';

  const suffix = 'v=1.2.3';
  const bundle = await build({
    css, dir, moreStyles, suffix,
  });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const linkPath = relative(dir, css);
  const styles = [moreStyles];
  t.plan(styles.length + 1);
  styles.map(style => t.regex(output, linkRegex(style)));
  t.regex(output, linkRegex(`${linkPath}\\?${suffix}`));
});

test('write html with one more script tag when `opts.moreScripts` is a string and suffix', async t => {
  const {
    dir,
    format,
    html,
    js,
  } = t.context;
  const moreScripts = 'https://www.example.com/lib.js';

  const suffix = 'v=1.2.3';
  const bundle = await build({ dir, moreScripts, suffix });
  await bundle.write({ format, file: js });

  const output = await fs.readFile(html, 'utf8');
  const scriptPath = relative(dir, js);
  const scripts = [moreScripts];
  t.plan(scripts.length + 1);
  scripts.map(script => t.regex(output, scriptRegex(script)));
  t.regex(output, scriptRegex(`${scriptPath}\\?${suffix}`));
});

test('error out when `opts.dir` is not given', async t => {
  t.plan(1);
  const { format, js: file } = t.context;
  const bundle = await build();
  await t.throws(bundle.write({ file, format }), /opts\.dir.*required/);
});

test('error out when output html cannot be written', async t => {
  t.plan(1);
  const { format, js: file } = t.context;
  // will get permission denied
  const bundle = await build({ dir: '/' });
  await t.throws(bundle.write({ file, format }), /EACCES/);
});
