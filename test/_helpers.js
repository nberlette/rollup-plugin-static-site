import { join, resolve } from 'path';

import fs from 'fs-extra';
import { rollup } from 'rollup';
import { directory as tempDir } from 'tempy';

import staticSite from '..';

// remove temp directory
export const afterEach = t => fs.remove(t.context.dir);

// setup context with temp directory and relevant paths
export const beforeEach = t => {
  const dir = tempDir();
  const format = 'iife';
  // eslint-disable-next-line no-param-reassign
  t.context = {
    dir,
    format,
    css: join(dir, 'css', 'styles.css'),
    html: join(dir, 'index.html'),
    js: join(dir, 'js', 'bundle.js'),
  };
};

// build using plugin
export const build = opts => rollup({
  input: resolve(__dirname, 'fixtures/entry.js'),
  plugins: [staticSite(opts)],
});

// help skip over attributes and whitespace
export const attrRegex = '[ \\w-=\'"/.]*';

// match a link tag with the given href
export const linkRegex = href => new RegExp(`<link${attrRegex}href="${href}"${attrRegex}>`);

// match a script tag with the given src
export const scriptRegex = src => new RegExp(`<script${attrRegex}src="${src}"${attrRegex}></script>`);

// custom template fixtures
const fixture = file => resolve(__dirname, 'fixtures', file);
export const templatePaths = {
  dot: fixture('dot.html'),
  dotCustomArrays: fixture('dot-custom-arrays.html'),
  ejs: fixture('ejs.html'),
  lodash: fixture('_.html'),
  mustache: fixture('mustache.html'),
  pug: fixture('pug.pug'),
  underscore: fixture('_.html'),
};
