module.exports = {
  extends: 'airbnb-base',
  rules: {
    'arrow-parens': ['error', 'as-needed'],
  },
};
