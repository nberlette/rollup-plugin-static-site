import { join } from 'path';
import browsersync from 'rollup-plugin-browsersync';
import buble from 'rollup-plugin-buble';
import copy from 'rollup-plugin-copy';
import { eslint } from 'rollup-plugin-eslint';
import filesize from 'rollup-plugin-filesize';
import postcss from 'rollup-plugin-postcss';
import staticSite from 'rollup-plugin-static-site';
import { terser } from 'rollup-plugin-terser';
import url from 'rollup-plugin-url';

import pkg from './package.json';

const IS_DEV = process.env.NODE_ENV === 'development';

const dest = (...args) => join('dist', ...args);
const src = (...args) => join('src', ...args);

const mainPaths = {
  css: {
    dest: dest('css', 'styles.css'),
  },
  html: {
    src: src('template.html'),
  },
  js: {
    dest: dest('js', 'bundle.js'),
    src: pkg.main,
  },
};

const moreScripts = [
  `https://unpkg.com/react@16/umd/react.${IS_DEV ? 'development' : 'production.min'}.js`,
  `https://unpkg.com/react-dom@16/umd/react-dom.${IS_DEV ? 'development' : 'production.min'}.js`,
];

export default {
  input: mainPaths.js.src,
  output: {
    file: mainPaths.js.dest,
    format: 'iife',
    name: 'js',
    sourcemap: !IS_DEV,
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM',
    },
  },
  plugins: [
    eslint({ include: src('**', '*.js') }),
    buble({ include: src('**', '*.js') }),
    url(),
    postcss({
      extract: IS_DEV ? false : mainPaths.css.dest,
      modules: true,
    }),
    copy({
      [src('react.ico')]: dest('favicon.ico'),
      verbose: true,
    }),
    staticSite({
      moreScripts,
      css: !IS_DEV && mainPaths.css.dest,
      dir: dest(),
      template: { path: mainPaths.html.src },
      title: `${pkg.name} app`,
    }),
    filesize(),
    IS_DEV && browsersync({ server: dest() }),
    !IS_DEV && terser(),
  ],
  external: ['react', 'react-dom'],
  watch: { include: src('**') },
};
