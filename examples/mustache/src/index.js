import './styles.css';

// eslint-disable-next-line no-console
const log = (...args) => console.log(`[${Date.now()}]`, ...args);
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const main = async function main() {
  await sleep(Math.round(Math.random() * 5000));
  log('hello');
  await sleep(Math.round(Math.random() * 5000));
  log('world');
};

main();
