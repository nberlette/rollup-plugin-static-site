import './styles.css';

const colors = {
  bg: [4, 58, 74],
  primary: [253, 174, 120],
  secondary: [226, 129, 161],
};
const maxCircleSize = 20;
const numCols = 16;
const numRows = 10;
const numStrands = 2;
const speed = 0.03;

let phase;
function setup() {
  phase = 0;

  noStroke();
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  phase = frameCount * speed;
  background(colors.bg);

  for (let strand = 0; strand < numStrands; strand += 1) {
    const strandPhase = phase + map(strand, 0, numStrands, 0, TWO_PI);

    for (let col = 0; col < numCols; col += 1) {
      const colOffset = map(col, 0, numCols, 0, TWO_PI);
      const x = map(col, 0, numCols, 50, width - 50);

      for (let row = 0; row < numRows; row += 1) {
        const y = height / 2 + row * 10 + sin(strandPhase + colOffset) * 50;
        const sizeOffset = (cos(strandPhase - row / numRows + colOffset) + 1) * 0.5;
        const circleSize = sizeOffset * maxCircleSize;

        const mixed = lerpColor(
          color(colors.primary),
          color(colors.secondary),
          row / numRows,
        );
        fill(mixed);
        ellipse(x, y, circleSize, circleSize);
      }
    }
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

// expose each function on window because rollup makes this an iife
window.setup = setup;
window.draw = draw;
window.windowResized = windowResized;
