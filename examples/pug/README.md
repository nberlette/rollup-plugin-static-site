# pug
rollup-plugin-static-site example for [pug](https://pugjs.org)

## notes
- `yarn build` creates a build in `dist` for production
- `yarn start` watches source files and starts browsersync for development
