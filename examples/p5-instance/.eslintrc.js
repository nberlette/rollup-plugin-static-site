module.exports = {
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  rules: {
    'arrow-parens': ['error', 'as-needed'],
    // oh, p5...
    'new-cap': 'off',
    'no-param-reassign': ['error', { props: false }],
    'no-new': 'off',
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      { optionalDependencies: true },
    ],
  },
};
