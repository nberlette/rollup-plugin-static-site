import dedent from 'dedent';

export const scriptsTemplate = [
  '{{~ it.scripts :script:i }}',
  '{{? !!script }}',
  '<script defer src="{{= script }}" type="text/javascript"></script>',
  // add newline and spaces for every element except last for pretty output by default
  '{{? i + 1 !== it.scripts.length }}\n      {{?}}',
  '{{?}}',
  '{{~}}',
];

export const stylesTemplate = [
  '{{~ it.styles :style:i }}',
  '{{? !!style }}',
  '<link href="{{= style }}" rel="stylesheet">',
  '{{? i + 1 !== it.styles.length }}\n      {{?}}',
  '{{?}}',
  '{{~}}',
];

export default dedent`
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>{{= it.title }}</title>
      ${stylesTemplate.join('')}
    </head>
    <body>
      ${scriptsTemplate.join('')}
    </body>
  </html>
`;
