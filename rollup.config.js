import isBuiltin from 'is-builtin-module';
import buble from 'rollup-plugin-buble';
import { eslint } from 'rollup-plugin-eslint';
import filesize from 'rollup-plugin-filesize';

import {
  dependencies,
  main as mainFile,
  module as moduleFile,
} from './package.json';

export default {
  input: 'src/index.js',
  output: [
    {
      file: mainFile,
      format: 'cjs',
      sourcemap: true,
    },
    {
      file: moduleFile,
      format: 'esm',
      sourcemap: true,
    },
  ],
  plugins: [
    eslint({
      include: 'src/**',
    }),
    buble({
      objectAssign: 'Object.assign',
      target: {
        node: 6,
      },
    }),
    filesize(),
  ],
  // don't bundle builtins or dependencies
  external: id => isBuiltin(id) || new Set(Object.keys(dependencies)).has(id),
};
